FROM ubuntu:latest

# https://hl2go.com/downloads/dedicated-servers/palworld/tutorials/how-to-set-up-a-palworld-dedicated-server-on-linux/
# https://github.com/A1RM4X/HowTo-Palworld/blob/main/README-no.steam.md

RUN apt-get update
RUN apt-get install software-properties-common -y

RUN apt-get install software-properties-common
RUN apt-add-repository main universe restricted multiverse
RUN dpkg --add-architecture i386
RUN apt-get update
RUN echo 2 | apt-get install steamcmd -y
RUN useradd -m steam
USER steam

RUN mkdir /home/steam/.steam && cd /home/steam/.steam && ln -s /home/steam/.local/share/Steam/steamcmd/linux32 sdk32 && ln -s /home/steam/.local/share/Steam/steamcmd/linux64 sdk64

RUN /usr/games/steamcmd +login anonymous +app_update 2394010 validate +quit
